# picoblogorg - a picolisp blog based off org files

Features:

1. A plain text (org) file for blog entries and configuration
2. Web interface for updating entries (version controlled through git)
3. Public/private posts

## pre-requisites

1. git 
2. mcrypt, base64, md5sum 

## setup

For security reasons, please change the following:

1. [key in crypt.sh](https://bitbucket.org/joebo/picoblogorg/src/69875c655e35880f0769fbff2fffbeb82c493bdd/crypt.sh?at=master)
2. [salt in org.l](https://bitbucket.org/joebo/picoblogorg/src/69875c655e35880f0769fbff2fffbeb82c493bdd/org-http.l?at=master)
3. [password in example.org](https://bitbucket.org/joebo/picoblogorg/src/213859d4c641fc422238b5864633548e4e217fd2/example.org) (if you use it)

## running

pil org-http.l -go  +

