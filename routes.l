
(de _authenticate (User Pass)
	(let (Users (org-to-users (read-org))
				User (car (seek '((X) (= (org-val 'Username (car X)) User)) Users))
				UserPass (org-val 'Password User)
				Hash (md5 Pass *Salt))
		(if (= UserPass Hash) User)))

(de login()
   (ifn *Post
		(renderPage "login.html")
		(let User (_authenticate *Username *Password)
			(unless User
				(renderPage "login.html" '(flash ("Invalid username or password"))))
			(when User
				(cookie 'Session (encrypt-string (str User)))
				(redirect "!article-list")))))

(de logout()
		(cookie 'Session NIL)
		(redirect "!login"))

(de article-list ()
	(let (Org (read-org)
              Blog (org-to-blog Org)
              Items (reverse (by '((X) (org-val 'date X)) sort Blog))
              Articles (if (get-username) Items (filter '((X) (= "Public" (org-val 'visibility X)))  Items)))
           (renderPage "list.html" (list 'org Articles))))
	

(de markup (Str)
	(let Line (pipe (out '(markdown) (prinl Str)) (pack (make (until (eof) (link (pack (line T) "^J"))))))
		Line))

(de article (p)
	(let (Org (read-org)
              Blog (org-to-blog Org)
              Items (reverse (by '((X) (org-val 'date X)) sort Blog))
              Articles (if (get-username) Items (filter '((X) (= "Public" (org-val 'visibility X)))  Items))
							Prev NIL
							Key NIL
							Next NIL
							Body NIL
							Auth NIL)
						(for (X Articles X (cdr X))
							(T (= (org-val 'slug (car X)) p) (prog (setq Next (cadr X)) (setq Article (car X))))
							(setq Prev (car X)))
					 (setq Key (org-val 'key Article))
					 (setq Body (markup (org-val 'body Article)))
					 (setq Auth (if (get-username) (list Key)))
           (renderPage "page.html" (list 'loggedin Auth 'Body Body 'article (list Article) 'next (list Next) 'prev (list Prev)))))

(de admin() 
	(let Username (get-username)
		(ifn Username
			(redirect "!login")))
   (when *Post
         (out File (prin *Text))
				 (chdir *Dir
					 (let Msg (in (list 'git "commit" "-a" "-m" "web update") (line))
						(out 2 (prin Msg)))))

  (let (Html (pack (readLines "org-html.html"))
         TemplateTree (parse Html)
         Org (read-org)
         Text (org-to-text Org)
         Model (cons 'org (list Org))
         HeaderTemplate (nth TemplateTree 4) )
         
         (setq Template TemplateTree)

         #need to find a more robust way to do this
         (push 'Model '((X) (if View (renderTree (cons 'org (list (list View))) HeaderTemplate))))
         (push 'Model 'orgt)
         (push 'Model (pipe (ht:Prin (pack Text)) (till NIL)))
         (push 'Model 'text)
         (httpHead NIL NIL)
         (prinl (renderTree Model TemplateTree))))


(de write-blog (Text)
 (out File (prin Text))
 (commit-blog))

(de commit-blog ()
 (chdir *Dir
	(let Msg (in (list 'git "commit" "-a" "-m" "web update") (line))
	 (out 2 (prin Msg)))))


(de blog-heading-to-visibility (Heading)
	(if (= Heading "Blog") "Public" "Private"))

(de article-edit (sID)
	(when (get-username)
	 (let (Org (org-flatten (read-org))
				 Heading (car (seek '((X) (= sID (text (org-val 'key (car X))))) Org))
				 ParentKey (org-val 'parentKey Heading)
				 ParentHeading (car (seek '((X) (= ParentKey (org-val 'key (car X)))) Org))
				 Visibility (blog-heading-to-visibility (org-val 'name ParentHeading))
				 Article (to-blog Heading Visibility)
				 Body (chop (pack (mapcar '((X) (pack X "^J"))  (cdr (org-val 'body Heading)))))
				 Body (pack (head (- (length Body) 1) Body)) # remove extra linefeed
				 ArticleModel (list 'key sID 'title (org-val 'title Article) 'body Body)) 
		(renderPage "post.html" (list 'Action "!article-update" 'Article (list ArticleModel))))))

(de article-new ()
	(when (get-username)
		(renderPage "post.html" (list 'Action "!article-create" 'Article (list 1)))))

(de _article-find-parent-key (Org Visibility)
 (let Heading (org-find-heading Visibility Org)
		(org-val 'key Heading)))

(de _article-make-entry (Existing)
	(let (Time (if Existing (org-val "CLOSED" (org-val 'special Existing))
								(pack "[" (dat$ (date) "-") " " (head 3 (chop (day (date)))) " " (tim$ (time)) "]"))
				Text (pack (diff (chop *Text) "^M"))
				Text (mapcar pack (split (chop Text) "^J"))
				Body (cons (pack "CLOSED: "  Time) Text))
		 (list 'level 2 'name *Title 'body Body children NIL 'props NIL)))

(de _article-new-id (Visibility)
 (let (Org (read-org)
			 Heading (org-find-heading Visibility Org)
			 NewHeading (car (org-val 'children Heading)))
	(org-val 'key NewHeading )))

(de article-create ()
	(when (and *Post (get-username))
		(let (Org (read-org)
					Visibility *Visibility
					ParentKey (_article-find-parent-key Org Visibility)
					Entry (_article-make-entry)
					NewID NIL
					NewOrg (org-add-child-by-key Org ParentKey Entry)
					Text (org-to-text NewOrg))
		 (write-blog Text)
		 (redirect (pack "!article-edit?" (_article-new-id Visibility))))))

(de article-update  ()
	(when (and *Post (get-username))
		(let (Org (org-flatten (read-org))
					sID *ID
					Heading (car (seek '((X) (= sID (text (org-val 'key (car X))))) Org))
					Key (org-val 'key Heading)
					Entry (_article-make-entry Heading)
					NewID NIL
					NewOrg (org-replace-by-key (read-org) Key Entry)
					Text (org-to-text NewOrg))
			 (write-blog Text)
			 (redirect (pack "!article-edit?" sID)))))

(de article-delete ()
	(when (and *Post (get-username))
		(let (Org (org-flatten (read-org))
					sID *ID
					Heading (car (seek '((X) (= sID (text (org-val 'key (car X))))) Org))
					Key (org-val 'key Heading)
					NewOrg (org-delete-by-key (read-org) Key)
					Text (org-to-text NewOrg))
			 (write-blog Text)
			 (redirect "!article-list"))))

